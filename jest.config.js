/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: './jest.domenvironment.js',
  testMatch: ['**/*.test.ts'],
  testPathIgnorePatterns: ['dist/', 'lib/'],
  modulePathIgnorePatterns: ['dist/'],
};
