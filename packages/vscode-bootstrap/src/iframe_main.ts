import { Config } from '@gitlab/web-ide-types';
import { createCommands } from '@gitlab/vscode-mediator-commands';
import { createError } from './utils/error';
import { createBufferWrapper } from './utils/createBufferWrapper';
import { getRepoRoot } from './utils/getRepoRoot';

const CONFIG_EL_ID = 'gl-config-json';

// amd is real yo
declare const define: any;

const getConfigFromDOM = (): Config => {
  const el = document.getElementById(CONFIG_EL_ID);

  if (!el) throw createError(`Could not find element for config in document (${CONFIG_EL_ID}).`);

  const json = el.dataset.settings;

  if (!json)
    throw createError(`Could not find "data-settings" in config element (${CONFIG_EL_ID}).`);

  return JSON.parse(json);
};

define('gitlab-vscode/main', [
  'vs/workbench/workbench.web.api',
  'vs/base/common/buffer',
  'vs/platform/log/common/log',
], (workbench: any, bufferModule: any, commonLogModule: any) => {
  const config = getConfigFromDOM();
  const repoRoot = getRepoRoot(config.projectPath);
  const fullConfig = {
    ...config,
    repoRoot,
  };

  // FIXME: Why not use the ConfigFromDOM?
  const extensionsUrl = document.getElementById('gl-extensions-base-url')?.dataset.settings;

  // example https://sourcegraph.com/github.com/microsoft/vscode@12aa93aacd80e844464b33dcac055c4c29f6d4e1/-/blob/src/vs/code/browser/workbench/workbench.ts?L521:2#tab=def
  workbench.create(document.body, {
    additionalBuiltinExtensions: [workbench.URI.parse(`${extensionsUrl}/gitlab-web-ide`)],
    developmentOptions: {
      // TODO: Make logLevel configurable
      logLevel: commonLogModule.parseLogLevel('trace'),
    },
    homeIndicator: {
      href: 'https://gitlab.com',
      icon: 'code',
      title: 'GitLab',
    },
    // TODO - Maybe we want this...
    welcomeBanner: undefined,
    commands: createCommands(fullConfig, createBufferWrapper(bufferModule)),
    defaultLayout: {
      views: [],
      editors: [],
      force: true,
    },
    settingsSyncOptions: {
      enabled: false,
    },
    productConfiguration: {
      // https://sourcegraph.com/github.com/sourcegraph/openvscode-server@3169b2e0423a56afba4fa1c824f966e7b3b9bf07/-/blob/product.json?L586
      // Gotta use open vsx! Otherwise we run into CORS issues with vscode :|
      extensionsGallery: {
        serviceUrl: 'https://open-vsx.org/vscode/gallery',
        itemUrl: 'https://open-vsx.org/vscode/item',
        resourceUrlTemplate:
          'https://open-vsx.org/vscode/asset/{publisher}/{name}/{version}/Microsoft.VisualStudio.Code.WebResources/{path}',
        controlUrl: '',
        recommendationsUrl: '',
      },
    },
    // This is needed so that we don't enter multiple workspace zone :|
    workspaceProvider: {
      workspace: { folderUri: workbench.URI.parse(`gitlab-vscode-ide:///${repoRoot}`) },
      trusted: true,
      async open() {
        return false;
      },
    },
  });
});
