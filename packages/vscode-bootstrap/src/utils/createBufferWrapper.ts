import { VSBufferWrapper } from '@gitlab/vscode-mediator-commands';

export const createBufferWrapper = (bufferModule: any): VSBufferWrapper => {
  return (actual: Uint8Array) => bufferModule.VSBuffer.wrap(actual);
};
