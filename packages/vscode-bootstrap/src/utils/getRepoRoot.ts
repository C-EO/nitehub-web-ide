export const getRepoRoot = (projectPath: string) => {
  const slashIdx = projectPath.lastIndexOf('/');
  if (slashIdx < 0) {
    return projectPath;
  } else {
    return projectPath.slice(slashIdx + 1);
  }
};
