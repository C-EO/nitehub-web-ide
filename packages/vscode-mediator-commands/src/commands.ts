import { GitLabClient } from '@gitlab/gitlab-api-client';
import { handleStartCommand } from './start';
import { ICommand, IFullConfig, VSBufferWrapper } from './types';

export const COMMAND_START = 'gitlab-vscode.start';
export const COMMAND_FETCH_FILE_RAW = 'gitlab-vscode.fetch-file-raw';
export const COMMAND_READY = 'gitlab-vscode.ready';

export const createCommands = (config: IFullConfig, bufferWrapper: VSBufferWrapper): ICommand[] => {
  const client = new GitLabClient({ baseUrl: config.gitlabUrl, authToken: config.gitlabToken });

  return [
    {
      id: COMMAND_START,
      handler: () => handleStartCommand(config, client),
    },
    {
      id: COMMAND_FETCH_FILE_RAW,
      handler: async (ref: string, path: string) => {
        const buffer = await client.fetchFileRaw(config.projectPath, ref, path);

        const byteArr = new Uint8Array(buffer);

        return bufferWrapper(byteArr);
      },
    },
    {
      id: COMMAND_READY,
      handler: () => {
        window.postMessage({ key: 'ready' });
      },
    },
  ];
};
