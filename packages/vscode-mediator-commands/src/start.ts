import { IFullConfig, StartCommandResponse } from './types';
import { GitLabClient } from '@gitlab/gitlab-api-client';

export const handleStartCommand = async (
  config: IFullConfig,
  client: GitLabClient,
): Promise<StartCommandResponse> => {
  const branch = await client.fetchProjectBranch(config.projectPath, config.ref);

  const files = await client.fetchFiles(config.projectPath, branch.commit.id);

  return {
    branch,
    files,
    repoRoot: config.repoRoot,
  };
};
