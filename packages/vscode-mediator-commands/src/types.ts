import { gitlab } from '@gitlab/gitlab-api-client';
import { Config } from '@gitlab/web-ide-types';

export interface IFullConfig extends Config {
  repoRoot: string;
}

export interface StartCommandResponse {
  files: string[];
  branch: gitlab.Branch;
  repoRoot: string;
}

export interface ICommand {
  id: string;
  handler: (...args: any[]) => unknown;
}

export interface VSCodeBuffer {
  readonly buffer: Uint8Array;
}

export type VSBufferWrapper = (arg0: Uint8Array) => VSCodeBuffer;
