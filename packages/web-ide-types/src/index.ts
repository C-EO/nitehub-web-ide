export interface Config {
  baseUrl: string;
  gitlabUrl: string;
  gitlabToken: string;
  projectPath: string;
  ref: string;
  nonce?: string;
}
