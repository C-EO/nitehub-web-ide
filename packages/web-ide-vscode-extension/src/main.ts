import * as vscode from 'vscode';
import { createSystems, FileList } from '@gitlab/web-ide-fs';
import { GitLabFileSearchProvider } from './vscode/GitLabFileSearchProvider';
import { start, ready } from './mediator/commands';
import { GitLabFileContentProvider } from './GitLabFileContentProvider';
import { FileSearcher } from './FileSearcher';
import { GitLabFileSystemProvider } from './vscode/GitLabFileSystemProvider';

const fetchContextTask =
  (context: vscode.ExtensionContext) =>
  async (
    progress: vscode.Progress<{ increment: number; message: string }>,
    token: vscode.CancellationToken,
  ) => {
    const result = await start();

    // New BrowserFS File System!!!
    const { fs, sourceControl } = await createSystems({
      contentProvider: new GitLabFileContentProvider(result.branch.commit.id),
      gitLsTree: result.files,
      repoRoot: result.repoRoot,
    });
    const fileList = new FileList(fs, result.repoRoot).withCache();
    const vscodeFs = new GitLabFileSystemProvider(fs);

    vscode.workspace.registerFileSystemProvider('gitlab-vscode-ide', vscodeFs, {
      isCaseSensitive: true,
      isReadonly: false,
    });
    vscode.workspace.registerFileSearchProvider(
      'gitlab-vscode-ide',
      new GitLabFileSearchProvider(new FileSearcher(fileList), result.repoRoot),
    );

    // TODO: We used to include the Spike implementation of SourceControl (inspired from Git).
    //       We've removed this since we moved over to the BrowserFS implementation of the FS.
    //       Look at commit 7f00b4f to view the old `git/` modules.
    // setupSCM(fs, git);

    // Refresh file view...
    vscode.commands.executeCommand('workbench.action.closeSidebar');
    vscode.commands.executeCommand('workbench.explorer.fileView.focus');

    await ready();
  };

export function activate(context: vscode.ExtensionContext) {
  console.log('HELLLOOO FROM GITLAB WORKBENCH!!!!');

  console.log(context.extensionUri);

  vscode.commands.executeCommand('workbench.action.closeAllEditors');

  vscode.window.withProgress(
    {
      cancellable: false,
      location: vscode.ProgressLocation.Notification,
      title: 'Initializing GitLab Web IDE...',
    },
    fetchContextTask(context),
  );
}

export function deactivate() {}
