import { IFileList } from '@gitlab/web-ide-fs';
import fuzzaldrinPlus from 'fuzzaldrin-plus';
import { IFileSearcher } from './types';

export class FileSearcher implements IFileSearcher {
  private readonly _fileList;

  constructor(fileList: IFileList) {
    this._fileList = fileList;
  }

  async searchBlobPaths(term: string, maxResults: number = 0): Promise<string[]> {
    const paths = await this._fileList.listAllBlobs();

    return fuzzaldrinPlus.filter(paths, term, {
      maxResults: maxResults <= 0 ? 10 : maxResults,
    });
  }
}
