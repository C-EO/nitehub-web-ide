import { gitlab } from './types';

const buildUrl = (baseUrl: string, ...parts: string[]) => {
  const relativePath = parts.map(encodeURIComponent).join('/');

  return new URL(relativePath, baseUrl).toString();
};

const withParams = (baseUrl: string, params: Record<string, string>) => {
  const paramEntries = Object.entries(params);

  if (!paramEntries.length) {
    return baseUrl;
  }

  const url = new URL(baseUrl);

  paramEntries.forEach(([key, value]) => {
    url.searchParams.append(key, value);
  });

  return url.toString();
};

export interface IGitLabClientConfig {
  baseUrl: string;
  authToken: string;
}

export class GitLabClient {
  private readonly _baseUrl: string;
  private readonly _authToken: string;

  constructor(config: IGitLabClientConfig) {
    this._baseUrl = config.baseUrl;
    this._authToken = config.authToken;
  }

  fetchProjectBranch(projectId: string, branchName: string): Promise<gitlab.Branch> {
    const url = this._buildApiUrl('projects', projectId, 'repository', 'branches', branchName);

    return this._fetchJson(url);
  }

  fetchFiles(projectId: string, ref: string): Promise<string[]> {
    const url = this._buildProjectUrl(projectId, '-', 'files', ref);

    return this._fetchJson(url, { format: 'json' });
  }

  async fetchFileRaw(projectId: string, ref: string, path: string): Promise<ArrayBuffer> {
    const url = this._buildApiUrl('projects', projectId, 'repository', 'files', path, 'raw');

    const response = await this._fetch(url, { ref });

    return response.arrayBuffer();
  }

  private _buildApiUrl(...parts: string[]) {
    return buildUrl(this._baseUrl, 'api', 'v4', ...parts);
  }

  private _buildProjectUrl(projectPath: string, ...parts: string[]) {
    return buildUrl(this._baseUrl, ...projectPath.split('/'), ...parts);
  }

  private _fetch(url: string, params: Record<string, string> = {}): Promise<Response> {
    return fetch(withParams(url, params), {
      method: 'GET',
      headers: this._getHeaders(),
    });
  }

  private async _fetchJson<T>(url: string, params: Record<string, string> = {}): Promise<T> {
    const response = await this._fetch(url, params);

    const result = await response.json();

    return result as T;
  }

  private _getHeaders(): Record<string, string> {
    if (!this._authToken) {
      return {};
    }

    return {
      'PRIVATE-TOKEN': this._authToken,
    };
  }
}
