import { getBaseUrlFromLocation } from './config';

describe('./config', () => {
  describe('getBaseUrlFromLocation', () => {
    it.each`
      input                                            | output
      ${'http://location:8000/path/to/foo/'}           | ${'http://location:8000/path/to/foo/web-ide/public'}
      ${'http://location:8000/path/to/foo/index.html'} | ${'http://location:8000/path/to/foo/web-ide/public'}
      ${'http://location:8000/'}                       | ${'http://location:8000/web-ide/public'}
      ${'http://location:8000/index.html'}             | ${'http://location:8000/web-ide/public'}
    `('with "$input", expects $output', ({ input, output }) => {
      (window as any).dom.reconfigure({ url: input });

      expect(getBaseUrlFromLocation()).toBe(output);
    });
  });
});
