export const getBaseUrlFromLocation = () => {
  const newUrl = new URL('web-ide/public', location.href);

  return newUrl.href;
};
