export const REPO_ROOT = 'test-repo';

export const DEFAULT_FILES = {
  'README.md': 'Lorem ipsum dolar sit\namit\n\n# Title\n123456\n',
  'foo/bar/index.js': 'console.log("Hello world!")\n',
  'foo/README.md': '# foo\n\nIt has foos.\n',
};
