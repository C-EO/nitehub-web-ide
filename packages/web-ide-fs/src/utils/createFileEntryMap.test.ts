import { FileType } from '../types';
import { createFileEntryMap, BlobContentType, IMutableTreeEntry, IMutableBlobEntry } from './index';

const convertMapToObj = <T>(map: Map<string, T>): Record<string, T> => {
  const result: Record<string, T> = {};

  for (const [key, value] of map.entries()) {
    result[key] = value;
  }

  return result;
};

const createTree = (name: string, children: string[]): IMutableTreeEntry => ({
  ctime: 0,
  mtime: 0,
  size: 0,
  type: FileType.Tree,
  name,
  children,
});

const createBlob = (name: string, path: string): IMutableBlobEntry => ({
  ctime: 0,
  mtime: 0,
  size: 0,
  type: FileType.Blob,
  name,
  content: {
    type: BlobContentType.Unloaded,
    path,
  },
});

describe('utils/createFileEntryMap', () => {
  it('with empty, creates empty root directory', () => {
    const result = createFileEntryMap([], '/');

    const actual = convertMapToObj(result);

    expect(actual).toEqual({
      '/': createTree('/', []),
    });
  });

  it('with ls-tree data, returns map of file entries', () => {
    const result = createFileEntryMap(['file', 'dir1/dir2/file2.js'], '/');

    const actual = convertMapToObj(result);

    expect(actual).toEqual({
      '/': createTree('/', ['/file', '/dir1']),
      '/dir1': createTree('dir1', ['/dir1/dir2']),
      '/dir1/dir2': createTree('dir2', ['/dir1/dir2/file2.js']),
      '/dir1/dir2/file2.js': createBlob('file2.js', 'dir1/dir2/file2.js'),
      '/file': createBlob('file', 'file'),
    });
  });

  it('with repoRoot, returns map of file entries nested under root', () => {
    const result = createFileEntryMap(['file', 'dir1/dir2/file2.js'], 'lorem-ipsum');

    const actual = convertMapToObj(result);

    expect(actual).toEqual({
      '/': createTree('/', ['/lorem-ipsum']),
      '/lorem-ipsum': createTree('lorem-ipsum', ['/lorem-ipsum/file', '/lorem-ipsum/dir1']),
      '/lorem-ipsum/dir1': createTree('dir1', ['/lorem-ipsum/dir1/dir2']),
      '/lorem-ipsum/dir1/dir2': createTree('dir2', ['/lorem-ipsum/dir1/dir2/file2.js']),
      // Path to blob content stays the same (not effected by repo root)
      '/lorem-ipsum/dir1/dir2/file2.js': createBlob('file2.js', 'dir1/dir2/file2.js'),
      '/lorem-ipsum/file': createBlob('file', 'file'),
    });
  });
});
