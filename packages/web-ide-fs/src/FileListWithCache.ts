import { IFileList, IFileSystem } from './types';

export class FileListWithCache implements IFileList {
  private readonly _fileList: IFileList;
  private readonly _fs: IFileSystem;

  private _cache: string[];
  private _cacheKey: number;

  constructor(fileList: IFileList, fs: IFileSystem) {
    this._fileList = fileList;
    this._fs = fs;

    this._cache = [];
    this._cacheKey = -1;
  }

  async listAllBlobs(): Promise<string[]> {
    return await this._updateCache();
  }

  private async _updateCache(): Promise<string[]> {
    const shouldUpdate = await this._shouldUpdateCache();

    if (!shouldUpdate) {
      return this._cache;
    }

    this._cache = await this._fileList.listAllBlobs();
    this._cacheKey = await this._generateCacheKey();
    return this._cache;
  }

  private async _shouldUpdateCache(): Promise<boolean> {
    const newKey = await this._generateCacheKey();

    return this._cacheKey !== newKey;
  }

  private _generateCacheKey(): Promise<number> {
    return this._fs.lastModifiedTime();
  }
}
