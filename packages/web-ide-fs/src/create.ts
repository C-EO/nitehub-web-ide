import { create } from './browserfs';
import { WebIdeFileSystemBFS } from './browserfs/WebIdeFileSystemBFS';
import { IFileContentProvider, ISystems } from './types';

export interface ICreateSystemOptions {
  contentProvider: IFileContentProvider;
  gitLsTree: string[];
  // TODO: Rename any instance of repo.?root or repo.?path to repoRootPath
  repoRoot: string;
}

export const createSystems = async (options: ICreateSystemOptions): Promise<ISystems> => {
  const { fs: browserfs, writable } = await create({
    contentProvider: options.contentProvider,
    gitLsTree: options.gitLsTree,
    repoRoot: options.repoRoot,
  });

  return {
    fs: new WebIdeFileSystemBFS({
      fs: browserfs,
      writableFS: writable,
      repoRootPath: options.repoRoot,
    }),
    // TODO: Create sourceControl that hooks into writeable fs
    sourceControl: {
      status() {
        return [];
      },
    },
  };
};
