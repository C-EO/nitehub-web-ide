- [Main Developer Docs](./README.md)
- [Working with packages](./packages.md)
- **[FAQ](./faq.md)**

# GitLab Web IDE - Developer FAQ

## My VSCode doesn't know how to open Plug-n-Play dependencies and I hate yarn 2. ?

Try install the ZipFS VSCode extension so that your VSCode editor can treat the dependencies
like the `.zip` are part of the native file system.

https://yarnpkg.com/getting-started/editor-sdks#vscode

## I'm adding a package and it doesn't work and I hate monorepos. ?

When adding (or removing) a new package, you will need to run `yarn install` for yarn to recognize the
workspace.

## Typescript isn't finding native JavaScript things and I hate Typescript. ?

Make sure that the package's `tsconfig.json` is extending the base config:

```json
{
  "extends": "../../tsconfig.base.json",
  "compilerOptions": {
    "composite": true,
    "outDir": "./lib",
    "rootDir": "./src"
  }
}
```

## How do I run the example with HTTPS?

1. Make sure you have valid certificate files named `cert.pem` and `key.pem` in the project's root directory.  
   You may want to create these outside the directory using something like `mkcert`. Then you can simply create
   symlinks in the project directory, with `ln -s <PATH_TO_CERT> ./cert.pem`.
2. Run `yarn start:example:ssl`.
