- **[Main Developer Docs](./README.md)**
- [Working with packages](./packages.md)
- [FAQ](./faq.md)

# GitLab Web IDE - Developer Docs

The GitLab Web IDE project is just your run-of-the-mill JavaScript monorepo using:

- [Typescript](https://www.typescriptlang.org/) with [composite project references](https://www.typescriptlang.org/docs/handbook/project-references.html#composite). Type safety is ensured during the project's `typescript-check` CI job.
- [Yarn 2+ Plug-n-play](https://yarnpkg.com/features/pnp) which powers the Yarn workspaces to work without symlinks.
  Plug'n'play removes `node_modules` and uses `yarn` itself to resolve `require(...)` statements at runtime.
  From a developer's perspective, this change is mostly seemless, with the exception that running node scripts with `node`
  should be replaced with `yarn node` (which automatically happens in Yarn scripts).
- [Yarn 2+ workspaces](https://yarnpkg.com/features/workspaces) which allows us to create internal packages, controlling
  coupling and cohesion at the package level. The `web-ide` package is the main public package which this project publishes.
- [Make](https://www.gnu.org/software/make/manual/html_node/Introduction.html) which manages build targets, dependencies, and
  recipes. Make is great at knowning **when** to incrementally build a new thing. In this project, Make is an implementation
  detail. Developers will typically just run [Yarn scripts](#scripts) that happen to internally call `make ...`.

## Setup

1. Make sure your system has all the commands needed (like `wget`) by checking out the [Brewfile](/Brewfile). If you are using
   [Homebrew](https://brew.sh/), then you can use [Homebrew Bundle](https://github.com/Homebrew/homebrew-bundle) and run
   `brew bundle` at the root of the project.
2. You'll need to [install yarn](https://yarnpkg.com/getting-started/install).
3. Run `yarn install`

To confirm that everything works:

4. Run `yarn run start:example`

## Setup (VSCode)

- For VSCode, tell VSCode to use the **workspace** version of TypeScript, by opening a `.ts` file and
  running the `TypeScript: Select TypeScript Version...`

## Scripts

Here's some scripts which can be run in the project root directory.

| Name                     | Description                                                        |
| ------------------------ | ------------------------------------------------------------------ |
| `yarn run build:ts`      | Builds and checks the typescript files.                            |
| `yarn run clean:ts`      | Cleans up typescript builds.                                       |
| `yarn run start:example` | Starts both the server and watched build of the `example` package. |

## Note about directory meanings

| Directory | Description                                               |
| --------- | --------------------------------------------------------- |
| `lib/`    | This is where we'll put typescript compilations if needed |
| `dist/`   | This is where we'll put actual bundled distributions      |
