# GitLab Web IDE

**STATUS:** In Development

A package for bootstrapping GitLab's context-aware Web IDE. Currently, this uses a browser build of [VSCode](https://github.com/microsoft/vscode).

## How to contribute?

Check out the [developer docs](./docs/dev/README.md).

## How to use the example?

You can run the example locally with `yarn start:example` or visit https://gitlab-org.gitlab.io/gitlab-web-ide/.

1. Fill out the startup configuration form with

   | Field        | Value                                                 |
   | ------------ | ----------------------------------------------------- |
   | GitLab URL   | `https://no-mors-cors.souldzin.com/https/gitlab.com/` |
   | Project Path | `gitlab-org/gitlab`                                   |
   | Ref          | `master`                                              |

   ![example](./docs/example_startup_config.png)

2. Click **Start GitLab Web IDE**

**PLEASE NOTE:**

- Until [this CORS issue](https://gitlab.com/gitlab-org/gitlab/-/issues/362532) is resolved, you'll need to proxy `gitlab.com` requests through [`no-mors-cors`](https://gitlab.com/souldzin/no-mors-cors) for this to work on the GitLab Pages deployment.

## References

Deployments:

- https://gitlab-org.gitlab.io/gitlab-web-ide/

Other projects:

- [GitLab VSCode Extension](https://gitlab.com/gitlab-org/gitlab-vscode-extension)
- [Spike MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/79096)
- [Spike playground project](https://gitlab.com/gitlab-org/frontend/playground/gitlab-vscode)
- [Temp playground project](https://gitlab.com/gitlab-org/frontend/playground/gitlab-vscode-prod)
